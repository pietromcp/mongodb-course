﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

// kljsadiljfnasfis!
// mongodbcourse
// mongodb+srv://mongodbcourse:<password>@gavia.c9wbm.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

namespace MongoDbFirstSample {
    class Program {
        static async Task Main(string[] args) {
            // BsonSerializer.RegisterSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document));
            BsonClassMap.RegisterClassMap<Item>(cm => {
                cm.SetIgnoreExtraElements(true);
                cm.AutoMap();
                cm.MapProperty(x => x.Timestamp).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                cm.MapProperty(x => x.Timestamp2).SetSerializer(new DateTimeOffsetSerializer(BsonType.Array));
                cm.MapProperty(x => x.Status).SetSerializer(new EnumSerializer<ItemStatus>(BsonType.String));
            });
            BsonClassMap.RegisterClassMap<Cat>(cm => { cm.AutoMap(); });
            BsonClassMap.RegisterClassMap<Dog>(cm => { cm.AutoMap(); });
            BsonClassMap.RegisterClassMap<Setting>(cm => {
                cm.AutoMap();
                cm.MapIdField(x => x.Key);
            });
            BsonClassMap.RegisterClassMap<CleanJobSetting>();
            BsonClassMap.RegisterClassMap<FileSystemSetting>();
            BsonClassMap.RegisterClassMap<ApiServiceSetting>();

            BsonClassMap.RegisterClassMap<StrangeDto>(cm => {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
                cm.MapProperty(x => x.Text);
                cm.MapProperty(x => x.Value);
                cm.MapCreator(x => new StrangeDto(x.Text, x.Value));
            });

            // MongoUrl url = MongoUrl.Create("mongodb://superadmin:sup3rs3cr3t@localhost/admin");
            // MongoUrl url = MongoUrl.Create("mongodb+srv://mongodbcourse:kljsadiljfnasfis!@gavia.c9wbm.mongodb.net/myFirstDatabase?retryWrites=true&w=majority");
            var client = new MongoClient("mongodb://localhost/test");
            // var cl2 = new MongoClient(new MongoClientSettings() {
            //     Server = MongoServerAddress.Parse("localhost"),
            //     Credential = new MongoCredential("SCRAM-SHA-256", new MongoInternalIdentity("admin", "superadmin"),
            //         new PasswordEvidence("sup3rs3cr3t"))
            // });
            // var db = client.GetDatabase("first");
            var db = client.GetDatabase("first");
            // var db = client.GetDatabase("myFirstDatabase");

            //await InsertMyFirstItem(db);
            //await HandleDtoWithoutSetters(db);
            //await GetAllItems(db);
            // await HandlePeople(db);
            // await UpdateItem(db);
            //await UpdatePeople(db);
            // await PersistHierarchy(db);
            await CustomSerialization.Play(db);
        }

        static async Task HandleDtoWithoutSetters(IMongoDatabase db) {
            var coll = db.GetCollection<StrangeDto>("strangery");
            await coll.DeleteManyAsync(Builders<StrangeDto>.Filter.Empty);
            await coll.InsertOneAsync(new StrangeDto("pietrom", 19));
            var reloaded = await (await coll.FindAsync(Builders<StrangeDto>.Filter.Empty)).SingleAsync();
            Console.WriteLine($"{reloaded.Text} | {reloaded.Value} | {reloaded.ComposedValue}");
        }


        static async Task PersistHierarchy(IMongoDatabase db) {
            var coll = db.GetCollection<Setting>("settings");
            // await coll.InsertManyAsync(new Setting[] {
            //     new ApiServiceSetting { Key = "ApiService0", Host = "localhost", Username = "us3r", Password = "p4ss" },
            //     new FileSystemSetting { Key = "FileSystem0", BaseDir = "/var/tmp" },
            //     new CleanJobSetting { Key = "CleanJob0", WorkingDir = "/var/log", CronExpr = "0 0 * * * *" },
            //     new CleanJobSetting { Key = "CleanJob", WorkingDir = "/var/tmp", CronExpr = "0 0 5 * * *" },
            // });
            var reloaded = await (await coll.FindAsync(Builders<Setting>.Filter.Empty)).ToListAsync();
            foreach (var setting in reloaded) {
                Console.WriteLine($"{setting.Key} --> {setting.GetType().AssemblyQualifiedName}");
            }
            Console.WriteLine("CleanJobSetting only:");
            var cleanJobSettings = await (await coll.FindAsync(Builders<Setting>.Filter.OfType<CleanJobSetting>())).ToListAsync();
            foreach (var setting in cleanJobSettings) {
                Console.WriteLine($"{setting.Key} --> {setting.GetType().AssemblyQualifiedName}");
            }
        }

        static async Task UpdatePeople(IMongoDatabase db) {
            var coll = db.GetCollection<Person>("people");
            var result = await coll.UpdateOneAsync(
                Builders<Person>.Filter
                    .Eq(x => x.Id, "pietrom"),
                Builders<Person>.Update
                    .Push(x => x.Pets, new Dog { Name = "Rintintin", CanSurviveWithoutOwner = false }));
            Console.WriteLine($"Updated documents count: {result.ModifiedCount}. Matched: {result.MatchedCount}");
        }

        static async Task UpdateItem(IMongoDatabase db) {
            var coll = db.GetCollection<Item>("items");
            var result = await coll.UpdateOneAsync(
                Builders<Item>.Filter
                    .Eq(x => x.Id, "key7"),
                Builders<Item>.Update
                    .Set(x => x.Text, "Updated text")
                    .Inc(x => x.BigValue, 3)
                    .Max(x => x.Value, 100)
            );
            Console.WriteLine($"Updated documents count: {result.ModifiedCount}. Matched: {result.MatchedCount}");
        }


        static async Task HandlePeople(IMongoDatabase db) {
            var coll = db.GetCollection<Person>("people");
            // await coll.InsertManyAsync(new[] {
            //     new Person {
            //         Id = "pietro",
            //         Pets = new Pet[] { new Cat { Name = "Neve", Age = 1 } }
            //     },
            //     new Person {
            //         Id = "pietrom",
            //         Pets = new Pet[] {
            //             new Cat { Name = "Neve", Age = 1 },
            //             new Cat { Name = "Ruspa", Age = 14 },
            //             new Cat { Name = "Sofia", Age = 12 }
            //         }
            //     }, new Person {
            //         Id = "martinellip",
            //         Pets = new Pet[] {
            //             new Cat { Name = "Neve", Age = 1 },
            //             new Dog { Name = "Rocky", CanSurviveWithoutOwner = false },
            //             new Dog { Name = "Pucci", CanSurviveWithoutOwner = true }
            //         }
            //     }
            // });
            var cursor = await coll.FindAsync(Builders<Person>.Filter.Empty);

            var people = await cursor.ToListAsync();
            foreach (var person in people) {
                Console.WriteLine($"Reloaded Person: {person.Id} --> [{string.Join(", ", person.Pets)}]");
            }
        }

        static Task InsertMyFirstItem(IMongoDatabase db) {
            var coll = db.GetCollection<Item>("items");
            Console.WriteLine($"Collection contains {coll.CountDocuments(Builders<Item>.Filter.Empty)} item(s)");
            return coll.InsertOneAsync(new Item {
                Id = "key8",
                Text = "text text 3212321",
                Value = 29,
                BigValue = 29L,
                DecimalValue = 29M,
                NowOClock = DateTime.Now,
                Timestamp = DateTimeOffset.Now,
                Timestamp2 = DateTimeOffset.Now,
                NestedProp = new Nested {
                    NestedText = "Text here!",
                    NestedBigValue = 1919L,
                    Recursive = new Nested {
                        NestedText = "AAA",
                        NestedBigValue = 171717L,
                        Recursive = null
                    }
                },
                Status = ItemStatus.InProgress
            });
            Console.WriteLine($"Collection contains {coll.CountDocuments(Builders<Item>.Filter.Empty)} item(s)");
        }

        static async Task GetAllItems(IMongoDatabase db) {
            var coll = db.GetCollection<Item>("items");
            var items = await coll.FindAsync(Builders<Item>.Filter.Empty);
            var texts = items.ToEnumerable().Select(x => x.Text);
            foreach (var item in await items.ToListAsync()) {
                Console.WriteLine($"FOund ITEM {item}");
            }
        }
    }

    class StrangeDto {
        public StrangeDto(string originalText, int baseValue) {
            Text = originalText;
            Value = baseValue;
            composedValue = $"{Text} --> {Value}";
        }

        private readonly string composedValue;

        public string Text { get; }
        public int Value { get; }
        public string ComposedValue => composedValue;
    }

    // [BsonIgnoreExtraElements]
    record Item {
        public string Id { get; set; }
        public string Text { get; set; }
        public int Value { get; set; }
        public long BigValue { get; set; }
        public decimal DecimalValue { get; set; }
        public DateTime NowOClock { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public DateTimeOffset Timestamp2 { get; set; }
        public Nested NestedProp { get; set; }
        public ItemStatus Status { get; set; }
    }

    public enum ItemStatus {
        New, InProgress, Failed, Completed
    }

    record Nested {
        public long NestedBigValue { get; set; }
        public string NestedText { get; set; }
        public Nested Recursive { get; set; }
    }

    class Person {
        public string Id { get; set; }
        public IList<Pet> Pets { get; set; }
    }

    interface Pet {
        string Name { get; }
    }

    class Cat : Pet {
        public string Name { get; set; }

        public int Age { get; set; }

        public override string ToString() {
            return $"<{nameof(Cat)}: {nameof(Name)}: {Name}, {nameof(Age)}: {Age}>";
        }
    }

    class Dog : Pet {
        public string Name { get; set; }

        public bool CanSurviveWithoutOwner { get; set; }

        public override string ToString() {
            return
                $"<{nameof(Dog)}: {nameof(Name)}: {Name}, {nameof(CanSurviveWithoutOwner)}: {CanSurviveWithoutOwner}>";
        }
    }

    abstract class Setting {
        public string Key { get; set; }
    }

    class ApiServiceSetting : Setting {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    class FileSystemSetting : Setting {
        public string BaseDir { get; set; }
    }

    class CleanJobSetting : Setting {
        public string WorkingDir { get; set; }
        public string CronExpr { get; set; }
    }
}