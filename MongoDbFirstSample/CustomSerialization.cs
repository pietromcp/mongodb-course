using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace MongoDbFirstSample {
    public class CustomSerialization {
        static CustomSerialization() {
            var pack = new ConventionPack { new CamelCaseElementNameConvention() };

            ConventionRegistry.Register(
                "My Custom Conventions",
                pack,
                t => t.Name == nameof(Account));

            BsonClassMap.RegisterClassMap<Account>(cm => {
                cm.AutoMap();
                cm.MapIdField(x => x.Id).SetSerializer(new AccountIdSerializer());
                // cm.MapField(x => x.Email).SetSerializer(new EmailAddressSerializer());
                cm.MapField(x => x.CreditCard).SetSerializer(new CreditCardNumberSerializer());
            });

            BsonClassMap.RegisterClassMap<Container>(cm => {
                cm.AutoMap();
                cm.MapField(x => x.Status).SetSerializer(new EnumSerializer<ContainerStatus>(BsonType.String));
                cm.MapField(x => x.When).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                // cm.MapField(x => x.When2).SetSerializer(new DateTimeSerializer());
            });

            BsonSerializer.RegisterSerializer<Bucket>(new BucketSerializer());
        }

        internal static async Task Play(IMongoDatabase db) {
            var coll = db.GetCollection<Container>("containers");
            // await coll.DeleteManyAsync(Builders<Container>.Filter.Empty);
            // await coll.InsertManyAsync(new[] {
            //     new Container { Id = "x", Status = ContainerStatus.First, When = DateTimeOffset.Now},
            //     new Container { Id = "y", Status = ContainerStatus.Second, When = DateTimeOffset.Now},
            //     new Container { Id = "z", Status = ContainerStatus.Third, When = DateTimeOffset.Now}
            // });
            var realoded = await (await coll.FindAsync(Builders<Container>.Filter.Empty)).ToListAsync();
            foreach (var container in realoded) {
                Console.WriteLine($"{container.Extras.Count}");
            }

            var original = new DateTime(2021, 5, 13, 16, 8, 26, DateTimeKind.Local);
            var changed = new DateTime(2021, 5, 13, 14, 8, 26, DateTimeKind.Utc);
            Console.WriteLine($"{original} == {changed}? {original == changed}");
            
            var originalDto = new DateTimeOffset(2021, 5, 13, 16, 8, 26, TimeSpan.FromHours(2));
            var changedDto = new DateTimeOffset(2021, 5, 13, 14, 8, 26, TimeSpan.FromHours(0));
            Console.WriteLine($"{originalDto} == {changedDto}? {originalDto == changedDto}");
        }

        internal static async Task Play2(IMongoDatabase db) {
            var coll = db.GetCollection<Account>("accounts");
            await coll.DeleteManyAsync(Builders<Account>.Filter.Empty);
            await coll.InsertManyAsync(new[] {
                new Account {
                    Id = new AccountId {
                        Tenant = new TenantId { Value = "aabb" },
                        IdInTenant = "pietrom"
                    },
                    Username = "pietrom",
                    CreatedAt = DateTimeOffset.Now,
                    Email = new EmailAddress { Value = "pietro@codiceplastico.com" },
                    CreditCard = new CreditCardNumber() { Value = "0000000000000000" },
                },
                new Account {
                    Id = new AccountId {
                        Tenant = new TenantId { Value = "aabb" },
                        IdInTenant = "martinellip"
                    },
                    Username = "martinellip",
                    CreatedAt = DateTimeOffset.Now,
                    Email = new EmailAddress { Value = "pietro.martinelli@codiceplastico.com" },
                    CreditCard = new CreditCardNumber() { Value = "1111111111111111" },
                }
            });
            var accounts = await (await coll.FindAsync(Builders<Account>.Filter.Empty)).ToListAsync();
            foreach (var reloaded in accounts) {
                Console.WriteLine(
                    $"Id: {reloaded.Id} Email: {reloaded.Email} Username: {reloaded.Username} CCN: {reloaded.CreditCard}");                
            }

            Console.WriteLine("--------------------");
            var filter = Builders<Account>.Filter.Eq(x => x.Email, new EmailAddress { Value = "pietro@codiceplastico.com" });

            Console.WriteLine($"filter as JSON {filter.ToJson()}");
            
            var reloadedByEmail = await (await coll.FindAsync(filter)).SingleAsync();
            Console.WriteLine(
                $"Id: {reloadedByEmail.Id} Email: {reloadedByEmail.Email} Username: {reloadedByEmail.Username} CCN: {reloadedByEmail.CreditCard}");
            Console.WriteLine("--------------------");
            var reloadedByEmail2 = await (await coll.FindAsync(Builders<Account>.Filter.Eq(x => x.Email.Value, "pietro@codiceplastico.com"))).SingleAsync();
            Console.WriteLine(
                $"Id: {reloadedByEmail2.Id} Email: {reloadedByEmail2.Email} Username: {reloadedByEmail2.Username} CCN: {reloadedByEmail2.CreditCard}");
            // var buckets = db.GetCollection<Bucket>("buckets");
            // await buckets.DeleteManyAsync(Builders<Bucket>.Filter.Empty);
            // await buckets.InsertOneAsync(new Bucket {
            //     Id = "b000",
            //     Text = "My text here!",
            //     Points = 100,
            //     BigValue = 191919L
            // });
            // var reloadedBucket = await (await buckets.FindAsync(Builders<Bucket>.Filter.Empty)).SingleAsync();
            // Console.WriteLine(reloadedBucket);
            // var replaced = await buckets.FindOneAndReplaceAsync(Builders<Bucket>.Filter.Eq(x => x.Id, "b000"),
            //     new Bucket {
            //         Id = "b000",
            //         Text = "My NEW text here!",
            //         Points = 101,
            //         BigValue = 191919191919L
            //     });
            // Console.WriteLine(replaced);
            //
            // var updated = await buckets.FindOneAndUpdateAsync(Builders<Bucket>.Filter.Eq(x => x.Id, "b000"),
            //     Builders<Bucket>.Update.Set(x => x.Text, "aaabbbcccddd"),
            //     new FindOneAndUpdateOptions<Bucket> { ReturnDocument = ReturnDocument.After });
            // Console.WriteLine(updated);  
            // var searchColl = db.GetCollection<Account>("research");
            // await searchColl.DeleteManyAsync(Builders<Account>.Filter.Empty);
            // await searchColl.InsertManyAsync(new [] {
            //     new Account { Email = new EmailAddress { Value = "pietrom@codiceplastico.com" }, Id = new AccountId { Tenant =  new TenantId {Value = "t" }, IdInTenant = "pietrom" }, Username = "pietrom",CreatedAt = DateTimeOffset.Now },
            //     new Account { Email = new EmailAddress { Value = "pietrom2@codiceplastico.com" }, Id = new AccountId { Tenant =  new TenantId {Value = "t2" }, IdInTenant = "pietrom2" }, Username = "pietrom2",CreatedAt = DateTimeOffset.Now },
            //     new Account { Email = new EmailAddress { Value = "pietrom3@codiceplastico.com" }, Id = new AccountId { Tenant =  new TenantId {Value = "t3" }, IdInTenant = "pietrom3" }, Username = "pietrom3",CreatedAt = DateTimeOffset.Now }
            // });
            // var pietrom = await (await searchColl.FindAsync(Builders<Account>.Filter.Eq(x => x.Email, new EmailAddress { Value = "pietrom3@codiceplastico.com" }))).SingleAsync();
            // Console.WriteLine($"Id: {pietrom.Id} Email: {pietrom.Email} Username: {pietrom.Username}");
        }
    }

    public class BucketSerializer : SerializerBase<Bucket> {
        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, Bucket value) {
            context.Writer.WriteStartDocument();
            context.Writer.WriteName("_lastWroteAt");
            context.Writer.WriteString(DateTimeOffset.Now.ToString());
            context.Writer.WriteName("_id");
            context.Writer.WriteString(value.Id);
            context.Writer.WriteName("txt");
            context.Writer.WriteString(value.Text);

            context.Writer.WriteName("ints");
            context.Writer.WriteStartDocument();
            context.Writer.WriteName("pts");
            context.Writer.WriteInt32(value.Points);
            context.Writer.WriteName("val");
            context.Writer.WriteInt64(value.BigValue);
            context.Writer.WriteEndDocument();

            context.Writer.WriteEndDocument();
        }

        public override Bucket Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args) {
            context.Reader.ReadStartDocument();
            var id = context.Reader.ReadString("_id");
            context.Reader.ReadName("_lastWroteAt");
            var lastWroteAt = DateTimeOffset.Parse(context.Reader.ReadString());

            var text = context.Reader.ReadString("txt");
            context.Reader.ReadName("ints");
            context.Reader.ReadStartDocument();
            var pts = context.Reader.ReadInt32("pts");
            var bigVal = context.Reader.ReadInt64("val");
            context.Reader.ReadEndDocument();
            context.Reader.ReadEndDocument();
            return new Bucket {
                Id = id,
                Points = pts,
                Text = text,
                BigValue = bigVal
            };
        }
    }

    public class AccountIdSerializer : SerializerBase<AccountId> {
        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, AccountId value) {
            Console.WriteLine("Serialize AccountId");
            context.Writer.WriteStartDocument();
            context.Writer.WriteName("t");
            context.Writer.WriteString(value.Tenant.Value);
            context.Writer.WriteName("a");
            context.Writer.WriteString(value.IdInTenant);
            context.Writer.WriteEndDocument();
        }

        public override AccountId Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args) {
            Console.WriteLine("Deserialize AccountId");
            context.Reader.ReadStartDocument();
            var nameDecoder = new Utf8NameDecoder();
            var tName = context.Reader.ReadName(nameDecoder);
            if (tName != "t") throw new Exception();
            var tenant = context.Reader.ReadString();
            var aName = context.Reader.ReadName(nameDecoder);
            if (aName != "a") throw new Exception();
            var idInTenant = context.Reader.ReadString();
            context.Reader.ReadEndDocument();
            return new AccountId() { Tenant = new TenantId { Value = tenant }, IdInTenant = idInTenant };
        }
    }

    // public class EmailAddressSerializer : SerializerBase<EmailAddress> {
    //     public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args,
    //         EmailAddress value) {
    //         var text = value.Value;
    //         context.Writer.WriteString(text);
    //     }
    //
    //     public override EmailAddress Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args) {
    //         var value = context.Reader.ReadString();
    //         return new EmailAddress { Value = value };
    //     }
    // }

    public class EmailAddressSerializer : WrapUnwrapSerializer<EmailAddress> {
        public EmailAddressSerializer() : base(email => email.Value, value => new EmailAddress { Value = value }) {
        }
    }

    public class CreditCardNumberSerializer : WrapUnwrapSerializer<CreditCardNumber> {
        public CreditCardNumberSerializer() : base(email => email.Value,
            value => new CreditCardNumber { Value = value }) {
        }
    }

    public record CreditCardNumber {
        public string Value { get; set; }
    }

    public class WrapUnwrapSerializer<T> : SerializerBase<T> {
        private readonly Func<T, string> unwrapper;
        private readonly Func<string, T> wrapper;

        public WrapUnwrapSerializer(Func<T, string> unwrapper, Func<string, T> wrapper) {
            this.unwrapper = unwrapper;
            this.wrapper = wrapper;
        }

        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args,
            T value) {
            var text = unwrapper(value);
            context.Writer.WriteString(text);
        }

        public override T Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args) {
            var value = context.Reader.ReadString();
            return wrapper(value);
        }
    }

    public class Account {
        public AccountId Id { get; set; }
        public string Username { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public EmailAddress Email { get; set; }
        public CreditCardNumber CreditCard { get; set; }
    }

    public class EmailAddress {
        public string Value { get; set; }
    }

    public record AccountId {
        public TenantId Tenant { get; set; }
        public string IdInTenant { get; set; }
    }

    public record TenantId {
        public string Value { get; set; }
    }

    public record Bucket {
        public string Id { get; set; }
        public string Text { get; set; }
        public long BigValue { get; set; }
        public int Points { get; set; }
    }

    public class Container {
        public string Id { get; set; }
        public DateTimeOffset When { get; set; }
        public ContainerStatus Status { get; set; }
        [BsonExtraElements]
        public IDictionary<string, object> Extras { get; set; }
    }

    public enum ContainerStatus {
        First, Second, Third
    }
}