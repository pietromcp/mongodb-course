﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace MongoDbSecondSample {
    class VariableSerializer : SerializerBase<Variable> {
        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, Variable value) {
            context.Writer.WriteStartDocument();
            context.Writer.WriteString("_id", value.Id.ToString());
            context.Writer.WriteString("_tt", value.Value.GetType().FullName);
            if (value.Value is short shortValue) {
                context.Writer.WriteInt32("_vv", shortValue);
            }
            if (value.Value is string stringValue) {
                context.Writer.WriteString("_vv", stringValue);
            }
            if (value.Value is decimal decimalValue) {
                context.Writer.WriteDecimal128("_vv", decimalValue);
            }
            context.Writer.WriteEndDocument();
        }

        public override Variable Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args) {
            context.Reader.ReadStartDocument();
            var id = context.Reader.ReadString("_id");
            var type = context.Reader.ReadString("_tt");
            var read = new Variable { Id = id };
            if (type == typeof(string).FullName) {
                read.Value = context.Reader.ReadString("_vv");
            }
            if (type == typeof(decimal).FullName) {
                read.Value = context.Reader.ReadDecimal128("_vv");
            }
            if (type == typeof(short).FullName) {
                var intValue = context.Reader.ReadInt32("_vv");
                read.Value = (short)intValue;
            }
            context.Reader.ReadEndDocument();
            return read;
        }
    }

    class Program {
        static Program() {
            // BsonClassMap.RegisterClassMap<Animale>(cm => {
            //     cm.AutoMap();
            // });
            // BsonClassMap.RegisterClassMap<Gatto>(cm => {
            //     cm.AutoMap();
            // });
            // BsonClassMap.RegisterClassMap<Cane>(cm => {
            //     cm.AutoMap();
            // });
            // BsonClassMap.RegisterClassMap<Mammifero>(cm => {
            //     cm.AutoMap();
            // });
            // BsonClassMap.RegisterClassMap<Pettirosso>(cm => {
            //     cm.AutoMap();
            // });
            BsonSerializer.RegisterSerializer(new VariableSerializer());
    }   


        static async Task Main(string[] args) {
            BsonClassMap.RegisterClassMap<Item>(cm => {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });
            
            var client = new MongoClient("mongodb://localhost/testdb");
            var db = client.GetDatabase("testdb22");
            // var db2 = client.GetDatabase("testdb44");
            // InsertItems(db);
            // ReadItems(db);
            
            // await InsertItemsAsync(db);
            //await ReadItemsAsync(db);
            // await InsertAndUpdate(db);
            // await InsertWithInheritance(db);
            // await InsertWithPrimitiveInheritance(db);
        }
    // 1978-03-19T01:15:00.000+01:00  Rome --> 19/03/1978 01:15
    // 1978-03-19T01:15:00.000+01:00  New York --> 18/03/1978 19:15
    // 1978-03-19T01:15:00.000+00:00
    // 1978-03-19T01:15:00.000+Z
    // UTC
    
    // 1978-03-19T01:15:00.000+01:00
    // 1978-03-19T06:15:00.000+06:00
    
    /*
     
     
     */
    
    
        private static async Task InsertWithPrimitiveInheritance(IMongoDatabase db) {
            var coll = db.GetCollection<Variable>("variables");
            await coll.DeleteManyAsync(Builders<Variable>.Filter.Empty);
            await coll.InsertManyAsync(new[] {
                new Variable { Id = "str", Value = "Pietro Martinelli" },
                new Variable { Id = "int", Value = 19 },
                new Variable { Id = "lon", Value = 19L },
                new Variable { Id = "dec", Value = 19M },
                new Variable { Id = "ush", Value = (ushort)19 },
                new Variable { Id = "sho", Value = (short)19 },
                new Variable { Id = "flo", Value = 11.17 },
                new Variable { Id = "boo", Value = true },
                new Variable { Id = "dto", Value = new DateTimeOffset(1978, 3, 19, 1, 15, 0, TimeSpan.FromHours(1)) }
            });
            var reloaded = await (await coll.FindAsync(Builders<Variable>.Filter.Empty)).ToListAsync();
            foreach (var variable in reloaded) {
                Console.WriteLine($"{variable.Id} {variable.Value?.GetType()?.Name} {variable.Value}");
            }
        }

        private static async Task InsertWithInheritance(IMongoDatabase db) {
            var coll = db.GetCollection<Animale>("animals");
            // await coll.DeleteManyAsync(Builders<CasaDegliAnimali>.Filter.Empty);
            // await coll.InsertOneAsync(new CasaDegliAnimali {
            //     Ospiti = new Animale[] {
            //         new Gatto { Id = "a", Nome = "Neve", MesiDiGravidanza = 1, NumeroDiSudditi = 3 },
            //         new Gatto { Id = "c", Nome = "Sofia", MesiDiGravidanza = 1, NumeroDiSudditi = 5 },
            //         new Cane { Id = "b", Nome = "Rocky", MesiDiGravidanza = 2, NomeDelPadrone = "Francesco" },
            //         new Mammifero { Id = "m", Nome = "Un mammifero", MesiDiGravidanza = 10 },
            //         new Pettirosso { Id = "p", Nome = "Usignolo", FrequenzaDiCanto = 100 }
            //     }
            // });

            var reloaded = await (await coll.FindAsync(
                // Builders<Animale>.Filter.OfType<Gatto>()
                // Builders<Animale>.Filter.Eq("_t", nameof(Gatto))
                Builders<Animale>.Filter.Eq("Nome", "Neve")
            )).ToListAsync();
            foreach (var animale in reloaded) {
                Console.WriteLine($"Nome: {animale.Nome} Tipo: {animale.GetType().Name}");
            }
        }
        
        


        private static async Task InsertAndUpdate(IMongoDatabase db) {
            var coll = db.GetCollection<Item>("updatable_items");
            var filter = Builders<Item>.Filter;
            var update = Builders<Item>.Update;
            await coll.DeleteManyAsync(filter.Empty);
            await coll.InsertManyAsync(new[] {
                new Item {Id = "a", Text = "Original text", BigValue = 19, OtherValue = 0, BoolValue = true},
                new Item {Id = "b", Text = "Original text", BigValue = 11, OtherValue = 1, BoolValue = true },
                new Item {Id = "c", Text = "Original text", BigValue = 17, OtherValue = 0, BoolValue = true },
                new Item {Id = "d", Text = "Original text", BigValue = 22, OtherValue = 1, BoolValue = false },
                new Item {Id = "e", Text = "Original text", BigValue = 6 , OtherValue = 0, BoolValue = false },
                new Item {Id = "f", Text = "Original text", BigValue = 21, OtherValue = 1, BoolValue = true },
            });

            UpdateResult result = await coll.UpdateManyAsync(
                filter.Gte(x => x.BigValue, 15),
                update
                    .Set(x => x.Text, "Updated value")
                    .Set(x => x.BoolValue, true)
                    .Inc(x => x.BigValue, 1)
                    .Inc(x => x.OtherValue, 100)
            );
            
            
            
            Console.WriteLine($"UpdateResult: {result.MatchedCount} {result.ModifiedCount}");

            var items = await (await coll.FindAsync(
                filter.Empty, new FindOptions<Item, Item2>() {
                    Projection = Builders<Item>.Projection
                        .Include(x => x.Id)
                        .Include(x => x.Text)
                        .Include(x => x.BigValue)
                }
            )).ToListAsync();
            Console.WriteLine($"Elements count {items.Count}");
            foreach (var item in items) {
                Console.WriteLine(item.Id);
            }
        }

        private static void ReadItems(IMongoDatabase db) {
            var collection = db.GetCollection<Item>("items");
            var cursor = collection.FindSync(Builders<Item>.Filter.Empty);
            while (cursor.MoveNext()) {
                foreach (var item in cursor.Current) {
                    Console.WriteLine(item);                    
                }
            }
            Console.WriteLine("==================");
            var cursor2 = collection.FindSync(Builders<Item>.Filter.Empty);
            var items = cursor2.ToList();
            foreach (var item in items) {
                Console.WriteLine(item);
            }
            Console.WriteLine("==================");
            var items2 = collection.AsQueryable()
                .Where(x => x.BigValue == 19)
                .ToList();
            foreach (var item in items2) {
                Console.WriteLine(item);
            }
        }
        
        private static async Task ReadItemsAsync(IMongoDatabase db) {
            var collection = db.GetCollection<Item>("items");
            var items = await collection.FindListAsync(Builders<Item>.Filter.Empty);
            foreach (var item in items) {
                Console.WriteLine(item);
            }
        }

        private static void InsertItems(IMongoDatabase db) {
            var collection = db.GetCollection<Item>("items");
            collection.InsertOne(new Item {
                Id = "ccc",
                Text = "Some text here",
                BigValue = 19,
                OtherValue = 11,
                BoolValue = true
            });
            var count = collection.CountDocuments(Builders<Item>.Filter.Empty); // {}
            Console.WriteLine($"Items in collection: {count}");
        }
        
        private static async Task InsertItemsAsync(IMongoDatabase db) {
            var collection = db.GetCollection<Item>("items");
            await collection.InsertOneAsync(new Item {
                Id = "eee",
                Text = "Some text here",
                BigValue = 19,
                OtherValue = 11,
                BoolValue = true
            });
            var count = await collection.CountDocumentsAsync(Builders<Item>.Filter.Empty); // {}
            Console.WriteLine($"Items in collection: {count}");
        }
    }

    record Item {
        public string Id { get; set; }
        public string Text { get; set; }
        public long BigValue { get; set; }
        public int OtherValue { get; set; }
        public bool BoolValue { get; set; }
    }

    record Item2 {
        public string Id { get; set; }
        public string Text { get; set; }
        public long BigValue { get; set; }
    }

    record PersistentItem {
        public long? Value { get; set; }
        public long? BigValue { get; set; }

        public static PersistentItem From(Item item) {
            return new PersistentItem {
                BigValue = item.BigValue
            };
        }

        public Item ToItem() {
            return new Item {
                BigValue = BigValue.HasValue ? BigValue.Value : Value.HasValue ? Value.Value : 0L
            };
        }
    }

    static class MongoCollectionExt {
        public static async Task<IList<T>> FindListAsync<T>(this IMongoCollection<T> collection, FilterDefinition<T> filter) {
            return await (await collection.FindAsync(filter)).ToListAsync();
        }
    }

    interface PeopleRepository {
        Task<Person> FindById(string id);

        Task Save(Person person);
    }

    class MongoDbPeopleRepository : PeopleRepository {
        private readonly IMongoCollection<Person> collection;

        public MongoDbPeopleRepository(MongoDatabase db) {
            collection = db.GetCollection<Person>("people");
        }

        public async Task<Person> FindById(string id) {
            var cursor = await collection.FindAsync(Builders<Person>.Filter.Eq(x => x.Id, id));
            return await cursor.SingleOrDefaultAsync();
        }

        public Task Save(Person person) {
            return collection.InsertOneAsync(person);
        }
    }

    record MongoDatabaseConfiguration {
        public MongoDatabaseConfiguration(string connectionString, string databaseName) {
            ConnectionString = connectionString;
            DatabaseName = databaseName;
        }

        public string ConnectionString { get; }
        public string DatabaseName { get; }
    }

    class MongoDatabase {
        private readonly MongoClient client;
        private readonly IMongoDatabase db;

        public MongoDatabase(MongoDatabaseConfiguration config) {
            client = new MongoClient(config.ConnectionString);
            db = client.GetDatabase(config.DatabaseName);
        }

        public IMongoCollection<T> GetCollection<T>(string collectionName) {            
            return db.GetCollection<T>(collectionName);
        }
    }

    class CustomerMongoDatabase : MongoDatabase {
        public CustomerMongoDatabase(MongoDatabaseConfiguration config) : base(config) {
        }
    }
    
    class CreditCardsMongoDatabase : MongoDatabase {
        public CreditCardsMongoDatabase(MongoDatabaseConfiguration config) : base(config) {
        }
    }

    class Person {
        public string Id { get; set; }
    }

    [BsonKnownTypes(typeof(Gatto), typeof(Cane), typeof(Pettirosso), typeof(Mammifero))]
    class Animale {
        public string Id { get; set; }
        public string Nome { get; set; }
    }

    class Pettirosso : Animale {
        public int FrequenzaDiCanto { get; set; }
    }
    
    class Mammifero : Animale {
        public int MesiDiGravidanza { get; set; }
    }

    class Gatto : Mammifero {
        public int NumeroDiSudditi { get; set; }
    }

    class Cane : Mammifero {
        public string NomeDelPadrone { get; set; }
    }

    class CasaDegliAnimali {
        public string Id { get; set; }
        public IEnumerable<Animale> Ospiti { get; set; }
    }

    class Variable {
        public object Id { get; set; }
        public object Value { get; set; }
    }
}